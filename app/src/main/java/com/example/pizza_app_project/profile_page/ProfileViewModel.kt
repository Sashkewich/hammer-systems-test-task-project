package com.example.pizza_app_project.profile_page

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class ProfileViewModel : ViewModel() {

    private val _text = MutableLiveData<String>().apply {
        value = "Экран 'Профиль'"
    }
    val text: LiveData<String> = _text
}