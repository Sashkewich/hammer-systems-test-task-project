package com.example.pizza_app_project.menu_page.di

import com.example.pizza_app_project.menu_page.api.MenuApi
import com.example.pizza_app_project.menu_page.interactor.MenuPageInteractor
import com.example.pizza_app_project.menu_page.repository.MenuRemoteRepository
import com.example.pizza_app_project.menu_page.repository.MenuRemoteRepositoryImpl
import dagger.Binds
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import retrofit2.Retrofit
import javax.inject.Qualifier
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
abstract class MenuPageModule {
    @Binds
    abstract fun bindMenuRemoteRepository(
        remoteRepository: MenuRemoteRepositoryImpl
    ): MenuRemoteRepository

    companion object {
        @Provides
        @Singleton
        fun provideApi(retrofit: Retrofit): MenuApi =
            retrofit.create(MenuApi::class.java)

        @Provides
        @Singleton
        fun provideInteractor(
            remoteRepository: MenuRemoteRepositoryImpl,
            @DefaultDispatcher defaultDispatcher: CoroutineDispatcher
        ): MenuPageInteractor =
            MenuPageInteractor(remoteRepository, defaultDispatcher)

        // Dispatchers Injection
        @IoDispatcher
        @Provides
        fun provideContextIo(): CoroutineDispatcher = Dispatchers.IO

        @DefaultDispatcher
        @Provides
        fun provideContextDefault(): CoroutineDispatcher = Dispatchers.Default

        @Retention(AnnotationRetention.RUNTIME)
        @Qualifier
        annotation class IoDispatcher

        @Retention(AnnotationRetention.RUNTIME)
        @Qualifier
        annotation class DefaultDispatcher

        @Retention(AnnotationRetention.RUNTIME)
        @Qualifier
        annotation class MainDispatcher

        @Retention(AnnotationRetention.BINARY)
        @Qualifier
        annotation class MainImmediateDispatcher
    }
}
