package com.example.pizza_app_project.menu_page.interactor

import com.example.pizza_app_project.menu_page.di.MenuPageModule.Companion.DefaultDispatcher
import com.example.pizza_app_project.menu_page.model.Result
import com.example.pizza_app_project.menu_page.repository.MenuRemoteRepositoryImpl
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.withContext
import javax.inject.Inject

class MenuPageInteractor @Inject constructor(
    private val remoteRepository: MenuRemoteRepositoryImpl,
    @DefaultDispatcher private val defaultDispatcher: CoroutineDispatcher
) {
    suspend fun getMenu(apiKey: String, query: String, number: Int): Result {
        return withContext(defaultDispatcher) {
            remoteRepository.getMenu(
                apiKey = apiKey,
                query = query,
                number = number
            )
        }
    }
}
