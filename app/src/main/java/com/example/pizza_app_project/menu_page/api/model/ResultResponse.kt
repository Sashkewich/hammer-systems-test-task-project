package com.example.pizza_app_project.menu_page.api.model

import com.google.gson.annotations.SerializedName

data class ResultResponse(
    @SerializedName("results")
    val menu: List<FoodResponse>,
)