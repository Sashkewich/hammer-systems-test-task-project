package com.example.pizza_app_project.menu_page.ui.adapter

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.pizza_app_project.R
import com.example.pizza_app_project.menu_page.model.Food

class MenuAdapter : RecyclerView.Adapter<MenuViewHolder>() {
    private val menu = mutableListOf<Food>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MenuViewHolder {
        LayoutInflater.from(parent.context).inflate(R.layout.menu_item, parent, false)
        return MenuViewHolder(parent)

    }

    override fun getItemCount(): Int = menu.size

    override fun onBindViewHolder(holder: MenuViewHolder, position: Int) {
        val listItem = menu[position]
        holder.onBind(listItem)
    }

    @SuppressLint("NotifyDataSetChanged")
    fun setMenu(items: List<Food>) {
        menu.clear()
        menu.addAll(items)
        notifyDataSetChanged()
    }
}