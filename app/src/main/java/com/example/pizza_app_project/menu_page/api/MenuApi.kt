package com.example.pizza_app_project.menu_page.api

import com.example.pizza_app_project.menu_page.api.model.ResultResponse
import retrofit2.http.GET
import retrofit2.http.Header
import retrofit2.http.Query

interface MenuApi {
    @GET("recipes/complexSearch")
    suspend fun getMenu(
        @Header("x-api-key") apiKey: String,
        @Query("query") query: String,
        @Query("number") number: Int,
    ): ResultResponse
}