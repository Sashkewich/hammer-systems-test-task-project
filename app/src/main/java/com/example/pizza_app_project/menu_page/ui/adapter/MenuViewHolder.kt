package com.example.pizza_app_project.menu_page.ui.adapter

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.pizza_app_project.R
import com.example.pizza_app_project.databinding.MenuItemBinding
import com.example.pizza_app_project.menu_page.model.Food
import com.example.pizza_app_project.utils.Constants

class MenuViewHolder(
    private val binding: MenuItemBinding,
) : RecyclerView.ViewHolder(binding.root) {
    constructor(
        parent: ViewGroup
    ) : this(MenuItemBinding.inflate(LayoutInflater.from(parent.context), parent, false))

    @SuppressLint("SetTextI18n")
    fun onBind(item: Food) {
        val foodImageSrc = item.image
        val foodName = item.title

        with(binding) {
            Glide.with(itemView).load(R.drawable.default_pizza_image).into(foodImage)
            foodNameText.text = foodName
            foodDescriptionText.text = foodName
            foodPriceText.text = Constants.FOOD_PRICE
        }
    }
}
