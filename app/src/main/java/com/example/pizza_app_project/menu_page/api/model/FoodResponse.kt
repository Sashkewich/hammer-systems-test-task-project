package com.example.pizza_app_project.menu_page.api.model

import com.google.gson.annotations.SerializedName

data class FoodResponse(
    @SerializedName("id")
    val id: Int,
    @SerializedName("image")
    val image: String,
    @SerializedName("imageType")
    val imageType: String,
    @SerializedName("title")
    val title: String
)