package com.example.pizza_app_project.menu_page.ui.viewmodel

import com.example.pizza_app_project.common.BaseViewModel
import com.example.pizza_app_project.menu_page.di.MenuPageModule.Companion.IoDispatcher
import com.example.pizza_app_project.menu_page.di.MenuPageModule.Companion.DefaultDispatcher
import com.example.pizza_app_project.menu_page.interactor.MenuPageInteractor
import com.example.pizza_app_project.menu_page.model.Result
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.withContext
import javax.inject.Inject

@HiltViewModel
class MenuViewModel @Inject constructor(
    private val interactor: MenuPageInteractor,
    @DefaultDispatcher val defaultDispatcher: CoroutineDispatcher,
    @IoDispatcher val ioDispatcher: CoroutineDispatcher
): BaseViewModel() {

    private val _menuStateFlow =
        MutableStateFlow(Result(emptyList()))
    val menuStateFlow = _menuStateFlow.asStateFlow()

    private val _loadingStateFlow = MutableStateFlow(false)
    val loadingStateFlow = _loadingStateFlow.asStateFlow()

    fun getMenu(apiKey: String, query: String, number: Int) {
        launch {
            withContext(ioDispatcher){
                val data = interactor.getMenu(apiKey, query, number)
                _menuStateFlow.emit(data)
            }
        }
    }
}