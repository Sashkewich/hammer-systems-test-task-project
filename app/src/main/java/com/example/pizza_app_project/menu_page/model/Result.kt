package com.example.pizza_app_project.menu_page.model

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class Result(
    val menu: List<Food>,
) : Parcelable