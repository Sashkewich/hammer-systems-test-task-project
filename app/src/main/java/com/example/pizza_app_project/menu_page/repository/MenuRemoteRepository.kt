package com.example.pizza_app_project.menu_page.repository

import com.example.pizza_app_project.menu_page.model.Result

interface MenuRemoteRepository {
    suspend fun getMenu(apiKey: String, query: String, number: Int): Result
}