package com.example.pizza_app_project.menu_page.repository

import com.example.pizza_app_project.menu_page.api.MenuApi
import com.example.pizza_app_project.menu_page.di.MenuPageModule.Companion.IoDispatcher
import com.example.pizza_app_project.menu_page.model.MenuModelConverter
import com.example.pizza_app_project.menu_page.model.Result
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.withContext
import javax.inject.Inject

class MenuRemoteRepositoryImpl @Inject constructor(
    private val api: MenuApi,
    @IoDispatcher private val ioDispatcher: CoroutineDispatcher
) : MenuRemoteRepository {
    override suspend fun getMenu(apiKey: String, query: String, number: Int): Result =
        withContext(ioDispatcher) {
            MenuModelConverter.convert(
                api.getMenu(
                    apiKey = apiKey,
                    query = query,
                    number = number
                )
            )
        }
}