package com.example.pizza_app_project.menu_page.ui.fragment

import android.annotation.SuppressLint
import android.view.View
import android.widget.ArrayAdapter
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.pizza_app_project.R
import com.example.pizza_app_project.common.BaseFragment
import com.example.pizza_app_project.databinding.FragmentMenuBinding
import com.example.pizza_app_project.menu_page.model.Food
import com.example.pizza_app_project.menu_page.ui.adapter.MenuAdapter
import com.example.pizza_app_project.menu_page.ui.viewmodel.MenuViewModel
import com.example.pizza_app_project.utils.Constants
import com.example.pizza_app_project.utils.extensions.viewbinding.viewBinding
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MenuFragment : BaseFragment(R.layout.fragment_menu) {
    private val binding: FragmentMenuBinding by viewBinding()
    private val viewModel: MenuViewModel by viewModels()
    private lateinit var layoutManager: LinearLayoutManager
    private val adapter = MenuAdapter()

    override fun bind() {
        with(viewModel) {
            observe(menuStateFlow){ data ->
                getMenu(Constants.API_KEY, Constants.PIZZA_TITLE, Constants.NUMBER)
                showMenu(data.menu)
            }
        }
    }

    @SuppressLint("ResourceType")
    override fun initViews(view: View) {
        with(binding){
            layoutManager = LinearLayoutManager(requireContext())
            menuRecyclerView.layoutManager = layoutManager
            menuRecyclerView.setHasFixedSize(true)
            menuRecyclerView.adapter = adapter
            adapter.onAttachedToRecyclerView(menuRecyclerView)
        }
    }

    private fun showMenu(data: List<Food>) {
        adapter.setMenu(data)
    }
}