package com.example.pizza_app_project.menu_page.model

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class Food(
    val id: Int,
    val image: String,
    val imageType: String,
    val title: String
) : Parcelable