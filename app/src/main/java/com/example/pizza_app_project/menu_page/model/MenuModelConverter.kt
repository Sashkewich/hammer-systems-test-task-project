package com.example.pizza_app_project.menu_page.model

import com.example.pizza_app_project.menu_page.api.model.FoodResponse
import com.example.pizza_app_project.menu_page.api.model.ResultResponse

object MenuModelConverter {
    fun convert(data: ResultResponse) =
        Result(
            menu = convertMenuList(data.menu)
        )

    private fun convertMenuList(list: List<FoodResponse>) =
        list.map {
            Food(
                id = it.id,
                image = it.image,
                imageType = it.imageType,
                title = it.title,
            )
        }
}