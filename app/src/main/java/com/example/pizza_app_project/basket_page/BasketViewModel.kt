package com.example.pizza_app_project.basket_page

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class BasketViewModel : ViewModel() {

    private val _text = MutableLiveData<String>().apply {
        value = "Экран 'Корзина'"
    }
    val text: LiveData<String> = _text
}