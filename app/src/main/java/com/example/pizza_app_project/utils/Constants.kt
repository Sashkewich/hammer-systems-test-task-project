package com.example.pizza_app_project.utils

object Constants {
    const val API_KEY = "3d8d107dc7de487ab41abbbd52faabed"
    const val NUMBER = 35
    const val PIZZA_TITLE = "pizza"
    const val FOOD_PRICE = "От 345 р"
    const val BASE_URL = "https://api.spoonacular.com/"
}